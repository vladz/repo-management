import re

# Builds a procmailrc statement to match commits for the given rule
def commits( rule ):
    # Start assembling the rule...
    procmailRule = []

    # Add an initial comment so we can track down which file/rule was involved in this statement first
    procmailRule.append("# File: %s" % rule['file'])
    procmailRule.append("# Rule: %s" % rule['name'])

    # Now open a new statement for Procmail
    # We want to carbon copy email so ensure it knows that too
    procmailRule.append(":0c")

    # Make sure we are dealing with a commit email first...
    procmailRule.append("* ^To: kde-commits@kde.org$")

    # Are we limiting this to a specific project?
    if 'project' in rule['where']:
        # Add the necessary filter...
        procmailRule.append("* ^X-Commit-Project: %s$" % rule['where']['project'])

    # Maybe we are branch filtering too?
    if 'branch' in rule['where']:
        # Add the necessary filter...
        procmailRule.append("* ^X-Commit-Ref: %s$" % rule['where']['branch'])

    # Otherwise could we be looking for a specific person?
    if 'author' in rule['where']:
        # Add this one too...
        procmailRule.append("* ^X-Commit-Pusher: %s$" % rule['where']['author'])

    # Finally add the line to forward the email to the person
    procmailRule.append("! %s" % rule['subscribes'])

    # Now convert the generated rule into something we can use and return it
    return "\n".join( procmailRule )

# Builds a procmailrc statement to match merge request mail for the given rule
def merge_requests( rule ):
    # Start assembling the rule...
    procmailRule = []

    # Add an initial comment so we can track down which file/rule was involved in this statement first
    procmailRule.append("# File: %s" % rule['file'])
    procmailRule.append("# Rule: %s" % rule['name'])

    # Now open a new statement for Procmail
    # We want to carbon copy email so ensure it knows that too
    procmailRule.append(":0c")

    # Make sure we are dealing with a merge request first...
    procmailRule.append("* ^X-GitLab-MergeRequest-ID: .*$")

    # Are we limiting this to a specific project?
    if 'project' in rule['where']:
        # Add the necessary filter...
        procmailRule.append("* ^X-GitLab-Project-Path: %s$" % rule['where']['project'])

    # Finally add the line to forward the email to the person
    procmailRule.append("! %s" % rule['subscribes'])

    # Now convert the generated rule into something we can use and return it
    return "\n".join( procmailRule )

# Builds a procmailrc statement to match task mail (both Phabricator and Gitlab) for the given rule
def tasks( rule ):
    # Start assembling the rule...
    procmailRule = []

    # Add an initial comment so we can track down which file/rule was involved in this statement first
    procmailRule.append("# File: %s" % rule['file'])
    procmailRule.append("# Rule: %s" % rule['name'])

    # Now open a new statement for Procmail
    # We want to carbon copy email so ensure it knows that too
    procmailRule.append(":0c")

    # Make sure we are dealing with a merge request first...
    procmailRule.append("* ^X-GitLab-Issue-ID: .*$")

    # Are we limiting this to a specific project?
    if 'project' in rule['where']:
        # Add the necessary filter...
        procmailRule.append("* ^X-GitLab-Project-Path: %s$" % rule['where']['project'])

    # Finally add the line to forward the email to the person
    procmailRule.append("! %s" % rule['subscribes'])

    # Now convert the generated rule into something we can use and return it
    return "\n".join( procmailRule )

# Builds a procmailrc statement to match bug traffic for the given rule
def bugs( rule ):
    # Start assembling the rule...
    procmailRule = []

    # Add an initial comment so we can track down which file/rule was involved in this statement first
    procmailRule.append("# File: %s" % rule['file'])
    procmailRule.append("# Rule: %s" % rule['name'])

    # Now open a new statement for Procmail
    # We want to carbon copy email so ensure it knows that too
    procmailRule.append(":0c")

    # Make sure we are dealing with a merge request first...
    procmailRule.append("* ^X-Bugzilla-URL: http://bugs.kde.org/$")

    # Are we limiting this to a specific product?
    if 'product' in rule['where']:
        # Add the necessary filter...
        procmailRule.append("* ^X-Bugzilla-Product: %s$" % rule['where']['product'])

    # Are we limiting this to a specific component?
    if 'component' in rule['where']:
        # Add the necessary filter...
        procmailRule.append("* ^X-Bugzilla-Component: %s$" % rule['where']['component'])

    # Are we limiting this to bugs filed on a specific operating system?
    if 'operating-system' in rule['where']:
        # Add the necessary filter...
        procmailRule.append("* ^X-Bugzilla-OperatingSystem: %s$" % rule['where']['operating-system'])

    # Finally add the line to forward the email to the person
    procmailRule.append("! %s" % rule['subscribes'])

    # Now convert the generated rule into something we can use and return it
    return "\n".join( procmailRule )


