import re
import sys
import email
import email.parser

# Prepare the email parser and load our message
mailParser = email.parser.Parser()
message    = mailParser.parse( sys.stdin )

# Any email that passes through Activity Filter is likely to be altered in some way or another
# Because some of this mail comes via mailing lists and may be headed to mailing lists, we should strip off a number of headers
# Otherwise we may have issues with DKIM Signatures further down the line
del message['DKIM-Signature']
del message['Authentication-Results']
del message['List-Id']
del message['List-Unsubscribe']
del message['List-Post']
del message['List-Help']
del message['List-Subscribe']
del message['Errors-To']
del message['Sender']

# Now we start checking to see if it is a Gitlab mail
# If it is not then we have nothing further to do here
if not 'X-GitLab-Reply-Key' in message:
    print( message.as_string() )
    sys.exit( 0 )

# Also make sure that it came from our instance of Gitlab
if not re.match( r'.*@invent.kde.org\>$', message.get('message-id', '') ):
    print( message.as_string() )
    sys.exit( 0 )

# Now that we know this is an email we should be mangling, let's get the stripping process underway
# Fortunately Gitlab helps us here by putting the security key by itself in a single header:
# X-GitLab-Reply-Key: 4c2d0b2...
# Therefore start by grabbing the security key
gitlab_security_key = message.get('x-gitlab-reply-key')
# Then remove it from the email...
del message['x-gitlab-reply-key']

# Once Gitlab emails have had their reply key removed the Reply-To address is useless, so remove it
del message['Reply-To']

# Now we go hunting across all the headers and obliterate the security key from it
# Reply-To: "KDE / Krita" <incoming+4c2d0b2...@invent.kde.org>
# References: <reply-4c2d0b2...@invent.kde.org> <merge_request_1344@invent.kde.org>
# List-Unsubscribe: <https://invent.kde.org/-/sent_notifications/4c2d0b2.../unsubscribe?force=true>,<mailto:incoming+4c2d0b2...-unsubscribe@invent.kde.org>
for header in message.keys():
    # Redact the key from the header....
    newheader = message.get( header, '' ).replace( gitlab_security_key, 'redacted' )
    # Then make sure it is used
    message.replace_header( header, newheader )

# That isn't the only place the security key can be found in though!
# We also have to go hunting in the HTML body of the message for it
# To do so we have to know the mailing list preferred character set....
# Start by going over all the parts of the message
for part in message.walk():
    # Determine if we have a HTML component here...
    # If not, skip to the next one
    ctype = part.get_content_type()
    if ctype != 'text/html' and ctype != 'text/plain':
        continue

    # Determine the character set this part is encoded in for later...
    charset = part.get_content_charset()
    # Grab the contents of the HTML component of the message
    oldpayload = part.get_payload(decode=True).decode(charset)

    # Perform the necessary replacement
    newpayload = oldpayload.replace( gitlab_security_key, 'redacted' )
    # Change the informative text explaining why they're receiving this email
    newpayload = newpayload.replace( "You're receiving this email because of your account on invent.kde.org.", "You're receiving this email through KDE Activity Filter (see sysadmin/activityfilter on invent.kde.org)" )
    # Remove the unsubscribe link completely...
    newpayload = newpayload.replace( "If you'd like to receive fewer emails, you can\n<a href=\"https://invent.kde.org/-/sent_notifications/redacted/unsubscribe\">unsubscribe</a>\nfrom this thread or\nadjust your notification settings.\n", "" )

    # Inject the updated contents
    part.set_payload( newpayload, charset )


# Finally print out our message and exit
print( message.as_string() )
sys.exit(0)
